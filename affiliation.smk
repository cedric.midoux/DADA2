rule addAffi:
	input:
		biom = "work/dada/dada.biom",
		fasta = "work/dada/seqtab.fasta"
	output:
		biom = "work/FROGS/affiliation.biom",
		html = "work/FROGS/affiliation.html",
		log = "work/FROGS/affiliation.log"
	threads:
		config["THREADS"]
	params:
		database = config["DATABASE"]
	shell:
		"conda activate frogs-3.2.0 "
		"&& "
		"affiliation_OTU.py "
		"--input-fasta {input.fasta} "
		"--input-biom {input.biom} "
		"--nb-cpu {threads} "
		"--log-file {output.log} "
		"--output-biom {output.biom} "
		"--summary {output.html} "
		"--reference {params.database} "
		"&& "
		"conda deactivate"

rule stats:
	input:
		biom = "work/FROGS/affiliation.biom"
	output:
		html_cluster = "work/FROGS/clusters_metrics.html",
		html_affiliations = "work/FROGS/affiliations_metrics.html"
	shell:
		"conda activate frogs-3.2.0 "
		"&& "
		"clusters_stat.py "
		"--input-biom {input.biom} "
		"--output-file {output.html_cluster}"
		"&& "
		"affiliations_stat.py "
		"--input-biom {input.biom} "
		"--output-file {output.html_affiliations} "
		"--taxonomic-ranks Domain Phylum Class Order Family Genus Species "
		"--rarefaction-ranks Genus "
		"--multiple-tag blast_affiliations "
		"--tax-consensus-tag blast_taxonomy "
		"--identity-tag perc_identity "
		"--coverage-tag perc_query_coverage"
		"&& "
		"conda deactivate"

rule tree:
	input:
		fasta = "work/dada/seqtab.fasta",
		biom = "work/FROGS/affiliation.biom"
	output:
		html = "work/FROGS/tree.html",
		nwk = "work/FROGS/tree.nwk",
		log = "work/FROGS/tree.log"
	threads:
		config["THREADS"]
	shell:
		"conda activate frogs-3.2.0 "
		"&& "
		"tree.py "
		"--nb-cpus {threads} "
		"--input-sequences {input.fasta} "
		"--biom-file {input.biom} "
		"--out-tree {output.nwk} "
		"--html {output.html} "
		"--log-file {output.log} "
		"&& "
		"conda deactivate"

rule stdBIOM:
	input:
		biom = "work/FROGS/affiliation.biom"
	output:
		biom = "work/FROGS/abundance.biom",
		tsv = "work/FROGS/blast_informations.std.tsv"
	shell:
		"conda activate frogs-3.2.0 "
		"&& "
		"biom_to_stdBiom.py "
		"--input-biom {input.biom} "
		"--output-biom {output.biom} "
		"--output-metadata {output.tsv}"
		"&& "
		"conda deactivate"

rule biom_to_tsv:
	input:
		biom = "work/FROGS/affiliation.biom",
		fasta = "work/dada/seqtab.fasta"
	output:
		tsv = "work/FROGS/abundance.tsv",
		multi = "work/FROGS/multihits.tsv"
	shell:
		"conda activate frogs-3.2.0 "
		"&& "
		"biom_to_tsv.py "
		"--input-biom {input.biom} "
		"--input-fasta {input.fasta} "
		"--output-tsv {output.tsv} "
		"--output-multi-affi {output.multi}"
		"&& "
		"conda deactivate"

localrules: copy
rule copy:
	input:
		"work/FROGS/{file}"
	output:
		"report/{file}"
	shell:
		"cp -uv {input} {output}"
