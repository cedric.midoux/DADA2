#!/bin/bash
export PYTHONPATH=''
source /usr/local/genome/Anaconda2-5.1.0/etc/profile.d/conda.sh
conda activate snakemake-5.7.4

mkdir -p report/

snakemake \
--snakefile $1 \
--jobscript jobscript.sh \
--cluster-config cluster.json \
--report report/`basename $1 .smk`_report.html \
--verbose
