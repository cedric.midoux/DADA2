rule cutadapt:
	input:
		"DATA/{sample}.fastq.gz"
	output:
		"work/cutadapt/{sample}.fastq.gz"
	params:
		five = lambda wildcards: config["FIVE_PRIMER"][wildcards.sample],
		three = lambda wildcards: config["THREE_PRIMER"][wildcards.sample]
	shell:
		"conda activate cutadapt-2.5 "
		"&& "
		"cutadapt "
		"-g {params.five} "
		"-a {params.three} "
		"--error-rate 0.1 "
		"--discard-untrimmed "
		"--match-read-wildcards "
		"-o {output} "
		"{input} "
		"&& "
		"conda deactivate"

rule filter:
	input:
		"work/cutadapt/{sample}.fastq.gz"
	output:
		"work/filter/{sample}.fastq.gz"
	script:
		"filterAndTrim.R"
