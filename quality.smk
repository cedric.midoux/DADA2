rule fastqc:
	input:
		"DATA/{sample}.fastq.gz"
	output:
		zip = "work/fastqc/{sample}_fastqc.zip",
		html = "work/fastqc/{sample}_fastqc.html"
	params:
		output = "work/fastqc/"
	shell:
		"conda activate fastqc-0.11.8 "
		"&& "
		"fastqc "
		"{input} "
		"--noextract "
		"--outdir {params.output} "
		"&& "
		"conda deactivate "

rule multiqc:
	input:
		expand("work/fastqc/{sample}_fastqc.zip", sample=SAMPLES)
	output:
		html = "report/multiqc_report.html",
	params:
		output = "report/"
	shell:
		"conda activate multiqc-1.8 "
		"&& "
		"multiqc "
		"--no-data-dir "
		"--outdir {params.output} "
		"{input} "
		"&& "
		"conda deactivate "
