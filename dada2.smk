rule dada2:
	input:
		"work/filter/{sample}.fastq.gz"
	output:
		"work/dada/{sample}.rds"
	threads:
		config["THREADS"]
	script:
		"dada2.R"

rule makeSequenceTable:
	input:
		expand("work/dada/{sample}.rds", sample=SAMPLES)
	output:
		rds = "work/dada/seqtab.rds",
		fasta = "work/dada/seqtab.fasta",
		tsv = "work/dada/seqtab.tsv",
		biom = "work/dada/dada.biom"
	threads:
		config["THREADS"]
	script:
		"makeSequenceTable.R"

rule metrics:
  input:
    rds = "work/dada/seqtab.rds",
    fastqc = expand("work/fastqc/{sample}_fastqc.zip", sample=SAMPLES)
  output:
    tsv = "work/dada/metrics.tsv"
  script:
    "metrics.R"
