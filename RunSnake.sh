#!/bin/bash
export PYTHONPATH=''
source /usr/local/genome/Anaconda2-5.1.0/etc/profile.d/conda.sh
conda activate snakemake-5.7.4

mkdir -p LOGS/

snakemake \
--snakefile $1 \
--jobscript jobscript.sh \
--cluster-config cluster.json \
--cluster "qsub -V -cwd -N {rule} -o {cluster.out} -e {cluster.err} -q {cluster.queue} -pe thread {threads} {cluster.cluster}" \
--keep-going \
--restart-times 5 \
--jobs 80 \
--wait-for-files \
--latency-wait 150 \
--verbose \
--printshellcmds

