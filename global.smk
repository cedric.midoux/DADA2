shell.executable("/bin/bash")
shell.prefix("source /usr/local/genome/Anaconda2-5.1.0/etc/profile.d/conda.sh;")

configfile: "./config.json"

SAMPLES=config["SAMPLES"]

rule all:
	input:
		"report/multiqc_report.html",
		expand("work/dada/{sample}.rds", sample=SAMPLES),
		"work/dada/seqtab.fasta",
		"work/dada/seqtab.tsv",
		"work/dada/metrics.tsv",
		"work/FROGS/affiliation.biom",
		"report/clusters_metrics.html",
		"report/affiliations_metrics.html",
		"report/abundance.biom",
		"report/abundance.tsv",
		# "report/tree.nwk"

include: "quality.smk"
include: "preprocess.smk"
include: "dada2.smk"
include: "affiliation.smk"
